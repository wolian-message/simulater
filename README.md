# CMPP/SMPP模拟器

[![SmsGateWay License](https://poser.pugx.org/simple-swoole/simps/license)](LICENSE)
[![Contact Us](https://img.shields.io/badge/contact-@Wowlian%20Team-blue.svg)](mailto:393323503@qq.com)
[![PHP Version](https://img.shields.io/badge/php-%3E=7.1-brightgreen.svg)](https://www.php.net)
[![Swoole Version](https://img.shields.io/badge/swoole-%3E=4.4.0-brightgreen.svg)](https://github.com/swoole/swoole-src)

🚀 SmsGateWay 是一个PHP + Swoole编写的轻量级，超高性能短信网关模拟器，相同配置下性能是Java模拟器(simpleTeam)的10倍，支持CmppV2协议(用于国内短信发送)和SmppV3.4协议(用于国际短信发送)，非常适合进行压力测试以及模拟收发短信。

#### 支持协议

* 国内CMPP协议
* 国际SMPP协议

#### ⚡️启动CMPP`仿真`模拟器

> 相对于`压测模拟器`，仿真模拟器会完全模拟真实短信发送场景，自动延迟状态报告，并产生随机错误码，并按照一定比例产生上行短信。

* 安装`Swoole4.4+`扩展和`PHP7.1+` 并`clone`本项目
* `cd`到 realCMPP 目录
* 参考`config.ini`配置文件注释，按需修改配置文件
* `php start_simulater.php`启动模拟器

#### ⚡️启动CMPP`压测`模拟器

> 压测模拟器在`2Core 4G`内存服务器，可以达到每秒上万的TPS。

* 安装`Swoole4.4+`扩展和`PHP7.1+` 并`clone`本项目
* `cd`到 benchCMPP 目录
* 参考`config.ini`配置文件注释，按需修改配置文件
* `php start_simulater.php`启动模拟器，CTRL+C结束服务，`nohup php start_simulater.php > log&`后台启动服务

> 需要CMPP,SGIP,SMGP压测客户端的可以[联系我们](https://wowlian.cn)

#### ⚡️启动SMPP网关模拟器
* 安装`Swoole4.4+`扩展和`PHP7.1+` 并`clone`本项目
* `cd`到 SMPP 目录
* 参考`config.ini`配置文件注释，按需修改配置文件
* `php start_simulater.php`启动模拟器
* `php smpp-client 1 2`启动客户端 第一个参数代表启动多少个连接 第二个参数代表发送多少条短信
> 此协议用于发送国际短信


#### TODO

* 目前没有校验cmpp connect参数

#### 开源协议

* 免责：本项目仅用于压测和模拟测试，禁止用于其他用途
* Apache License Version 2.0 see http://www.apache.org/licenses/LICENSE-2.0.html
